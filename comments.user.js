// ==UserScript==
// @name            Show comments actions by default
// @namespace       https://embers.ml/@deitels
// @description     Test script
// @include         https://www.taringa.net/*
// @require         https://code.jquery.com/jquery-3.2.1.min.js
// @version         0.1
// ==/UserScript==
//

window.setInterval(function () {
  $(".comment-actions").show();
}, 1000);
