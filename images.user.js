// Copyright 2017 Lala-Ru <lala-ru@keemail.me>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ==UserScript==
// @name            Proxy images
// @namespace       https://www.taringa.net/Lala-Ru
// @description     Use image proxy to view images of Taringa
// @include         https://www.taringa.net/*
// @require         https://code.jquery.com/jquery-3.2.1.min.js
// @version         1.4.2
// ==/UserScript==

var imageproxy = "https://images.weserv.nl/?url=";
var gifurl = "https://s4.ezgif.com/optimize";

window.setInterval(function() {
    $("a.webm-js").each(function(i, obj) {
        var video = $(obj).find("video");
        var sentinel = $(video).attr("preload");
        if ((typeof video !== typeof undefined && video !== false) && sentinel !== "lalaru") {
            var url = video.attr("poster");
            url = url.replace(/\.cover(\?+)?$/, "");
            $(video).hide();
            $(obj).append("<img></img>");
            $(obj).find("img").attr("src", url);
            $(obj).find(".webm-js_gif").hide();
            $(video).attr("preload", "lalaru");
        };
    });

    $("img").each(function(i, obj) {
        var orig = $(obj).attr("orig");
        var url = "";
        if (typeof orig !== typeof undefined && orig !== false) {
            url = orig;
        } else {
            url = $(obj).attr("src");
        };
        url = url.replace(/^.+(\w+:|)\/\//, '');
        if (!(url.startsWith("images.weserv.nl") || url.startsWith("im4.ezgif.com"))) {
            var oldurl = url;
            url = imageproxy + url;
            $(obj).attr("src", url);
            if (oldurl.match("\.gif(\\?+)?$")) {
                $.post(gifurl, {"new-image-url": oldurl, "new-image": ""}, function(data, statuscode, response) {
                    if (typeof data !== typeof undefined && data !== false) {
                        $(obj).attr("src", "https://" + data.match("im4.ezgif.com\/tmp\/[a-zA-Z0-9-]+\.gif"))
                    };
                });
            };
        };
    });
}, 3000);
